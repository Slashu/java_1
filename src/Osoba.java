import java.util.Random;

/**
 * Created by T420 on 2017-06-28.
 */
public abstract class Osoba {
    private final String pesel;
    private String imie;
    private String nazwisko;
    protected static StringBuilder sb = new StringBuilder();
    protected static Random rand = new Random();


    public Osoba() {
        this.pesel = randomPesel();
        sb.delete(0,sb.length());
        this.imie = randomImie();
        sb.delete(0,sb.length());
        this.nazwisko = randomNazwisko();
        sb.delete(0,sb.length());
    }

    public Osoba(String pesel, String imie, String nazwisko) {
        this.pesel = pesel;
        this.imie = imie;
        this.nazwisko = nazwisko;
    }

    public String randomPesel() {
        for (int i = 0; i < 10; i++) {
            sb.append((rand.nextInt(10)));
        }
        return ((sb.toString().substring(0, 1).toUpperCase() + sb.toString().substring(1)));
    }

    public String randomImie() {
        for (int i = 0; i < 5; i++) {
            sb.append((char) (rand.nextInt(26) + 'a'));
        }
        //System.out.println(sb.toString().substring(0,1).toUpperCase()+sb.toString().substring(1));
        return (sb.toString().substring(0, 1).toUpperCase() + sb.toString().substring(1));
    }

    public String randomNazwisko() {
        for (int i = 0; i < 8; i++) {
            sb.append((char) (rand.nextInt(26) + 'a'));
        }
        //System.out.println(sb.toString().substring(0,1).toUpperCase()+sb.toString().substring(1));
        return (sb.toString().substring(0, 1).toUpperCase() + sb.toString().substring(1));
    }


    public String getPesel() {
        return pesel;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }
}
