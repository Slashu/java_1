/**
 * Created by T420 on 2017-06-30.
 */
public class Dzial implements Comparable<Dzial> {
    private String nazwaDz;

    public Dzial(String nazwaDz) {
        this.nazwaDz = nazwaDz;
    }

    public String getNazwaDz() {
        return nazwaDz;
    }

    public void setNazwaDz(String nazwaDz) {
        this.nazwaDz = nazwaDz;
    }

    @Override                                       //tu byl blad
    public int compareTo(Dzial o) {
        return this.nazwaDz.compareTo(o.nazwaDz);

    }
}
