import java.util.Comparator;

/**
 * Created by T420 on 2017-06-29.
 */
public class MyComparator implements Comparator<Pracownik> {

    @Override
    public int compare(Pracownik o1, Pracownik o2) {
        return Double.compare(o1.getPensja(), o2.getPensja());
    }
}
