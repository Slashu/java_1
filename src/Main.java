import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

public class Main {

    public static void main(String[] args) {


        Firma f1 = new Firma();

        //Pracownik p1 = new Pracownik("1029310492","Janusz","Kowalski",1000,"kkk");
        /*System.out.println(p1.toString());
        p1.setRandomPesel();
        System.out.println(p1.toString());*/


        /*Pracownik px = new Pracownik().randomize();
        System.out.println(px.toString());*/



        for (int i = 0; i <50000 ; i++) {
            f1.dodajLosowegoPracownika();
        }

        /*
        long t1= System.nanoTime();
        f1.sortPrac1();
        //f1.wyswietlPracownikowFull();
        System.out.println("czas1 = "+(System.nanoTime()-t1)/1000000);

        long t2=System.nanoTime();
        f2.sortPrac2();
        System.out.println("czas2 = "+(System.nanoTime()-t2)/1000000);
        //f2.wyswietlPracownikowFull();
*/
        List<Pracownik> listaPracownikow = f1.getListaPrac();
        for (Pracownik pracownik : listaPracownikow) {
            pracownik.addDzial(100);
        }

        long t3=System.nanoTime();
        f1.sortPrac3();
        for (Pracownik pracownik : listaPracownikow) {
            pracownik.sortDzial3();
        }
        System.out.println("czas2 = "+(System.nanoTime()-t3)/1000000+" ms");
    }
}
