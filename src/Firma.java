import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by T420 on 2017-06-28.
 */
public class Firma {
    private List<Pracownik> listaPrac = new ArrayList<>();

    public void dodajLosowegoPracownika(){

        listaPrac.add(new Pracownik());
    }

    public List<Pracownik> getListaPrac() {
        return listaPrac;
    }

    public void wyswietlPracownikow(){
        for (int i = 0; i < listaPrac.size() ; i++) {
            System.out.println(listaPrac.get(i).toString());
        }
    }

    public void wyswietlPracownikowFull(){
        for (int i = 0; i < listaPrac.size() ; i++) {
            System.out.println(listaPrac.get(i).toString()+" Pensja: "+listaPrac.get(i).getPensja()+" Stanowisko: "+listaPrac.get(i).getStanowisko());
        }
    }

    public void wyswietlPensje(){
        for (int i = 0; i < listaPrac.size() ; i++) {
            System.out.println(listaPrac.get(i).getPensja());
        }
    }

    public void wyswietlStanowiska(){
        for (int i = 0; i < listaPrac.size() ; i++) {
            System.out.println(listaPrac.get(i).getStanowisko());
        }
    }

    public void sortPrac1(){

        listaPrac.sort(Comparator.comparing(Pracownik::getPensja));
    }
    public void sortPrac2(){
        for (int i = 0; i <listaPrac.size() ; i++) {
            Collections.sort(listaPrac, new MyComparator());
        }
    }

    public void sortPrac3(){
        Collections.sort(listaPrac);
    }


    public Pracownik getPracLista(int x){
        return listaPrac.get(x);
    }

    public int getIloscPrac(){
        return listaPrac.size();
    }
}
