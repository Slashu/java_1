/**
 * Created by T420 on 2017-06-28.
 */
public class Szef extends Pracownik{

    public Szef(String pesel, String imie, String nazwisko, double pensja, String stanowisko) {
        super(pesel, imie, nazwisko, pensja, stanowisko);
    }

    public void dajPodwyzke(int procent, Pracownik px){
        px.setPensja(px.getPensja()*((double)procent+100)/100);
    }
}
