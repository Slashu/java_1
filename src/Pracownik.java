import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by T420 on 2017-06-28.
 */
public class Pracownik extends Osoba implements Comparable<Pracownik> {
    private String stanowisko;
    private double pensja;
    private List<Dzial> listaDzial = new ArrayList<>();

    public Pracownik() {
        this.pensja = randomPensja();
        this.stanowisko = randomStanowisko();
        sb.delete(0, sb.length());
    }

    public Pracownik(String pesel, String imie, String nazwisko, double pensja, String stanowisko) {
        super(pesel, imie, nazwisko);
        this.setPensja(pensja);
        this.setStanowisko(stanowisko);
    }

    public String getStanowisko() {
        return stanowisko;
    }

    public void setStanowisko(String stanowisko) {
        this.stanowisko = stanowisko;
    }

    public double getPensja() {
        return pensja;
    }

    public void setPensja(double pensja) {
        this.pensja = pensja;
    }

    @Override
    public String toString() {
        return "Pesel: " + getPesel() + " Imie: " + getImie() + " Nazwisko: " + getNazwisko();
    }

    /*public void wyswietlPensje(){
        System.out.println(this.getPensja());
    }
    public void wyswietlStanowisko(){
        System.out.println(this.getStanowisko());
    }*/


    public String randomStanowisko() {
        for (int i = 0; i < 5; i++) {
            sb.append((char) (rand.nextInt(26) + 'a'));
        }
        //System.out.println(sb.toString().substring(0,1).toUpperCase()+sb.toString().substring(1));
        return (sb.toString().substring(0, 1).toUpperCase() + sb.toString().substring(1));
    }

    public double randomPensja() {
        return (1000 + (rand.nextDouble() * 3000) / 3);
    }

    /*public Pracownik randomize() {
        return new Pracownik(randomPesel(), randomImie(), randomNazwisko(), randomPensja(), randomStanowisko());
    }*/

    public void addDzial(int x) {
        sb.delete(0, sb.length());
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < 5; j++) {
                sb.append((char) (rand.nextInt(26) + 'a'));
            }
            //System.out.println(sb.toString());
            listaDzial.add(new Dzial(sb.toString()));
            sb.delete(0, sb.length());
        }
    }

    public void sortDzial1() {
        listaDzial.sort(Comparator.comparing(Dzial::getNazwaDz));
    }

    public void sortDzial3() {
        Collections.sort(listaDzial);
    }

    @Override
    public int compareTo(Pracownik o) {
        return ((int) this.pensja - (int) o.pensja);
    }



    /*public Pracownik randomize() {
        Random random = new Random();
        StringBuffer sb = new StringBuffer();
        int ii = random.nextInt(5);
        String imie1="";
        for (int i = 0; i < ii; i++) {
            sb.append((char) random.nextInt(26) + 'a');

        }
        imie1 = sb.toString();
        sb.delete(0, sb.length());
        ii = random.nextInt(7);
        for (int i = 0; i < ii; i++) sb.append(Character.toString(((char)random.nextInt(26) + 'a')));
        String nazwisko = sb.toString();
        sb.delete(0, sb.length());
        for (int i = 0; i < 10; i++) sb.append(random.nextInt(10));
        String pesel = sb.toString();
        sb.delete(0, sb.length());
        double pensja = (random.nextDouble() * 30000) / 3;
        for (int i = 0; i < 5; i++) sb.append((char) random.nextInt(26) + 'a');
        String stanowisko = sb.toString();
        Pracownik px = new Pracownik(pesel, imie1, nazwisko, pensja, stanowisko);
        return px;
    }*/


}
